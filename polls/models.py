import datetime

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone

@python_2_unicode_compatible
class Question(models.Model):
    """
    Question Model
    """
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        """
        to string
        return question_text field
        """
        return self.question_text

    def was_published_recently(self):
        """
        demo method
        """
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.shord_description = 'Published recently?'


@python_2_unicode_compatible
class Choice(models.Model):
    """
    Question Choice Model
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        """
        to string
        return choice_text field
        """
        return self.choice_text
